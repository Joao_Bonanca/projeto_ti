<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/logout', 'Auth\LoginController@logout')->name('logout' );

// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/andares', function () {
    return view('frontend.andares0');
})->name('andares0');

Route::get('/andares1', function () {
    return view('frontend.andares1');
})->name('andares1');

Route::get('/andares2', function () {
    return view('frontend.andares2');
})->name('andares2');