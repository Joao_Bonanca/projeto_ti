<!DOCTYPE html>
<html lang="pt">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- <title>{{ config('app.name', 'Projeto de TI') }}</title> -->
    <title>Projeto TI</title>
    <link rel="stylesheet" href="css/app.css">
    <link href="{{ url('fontawesome-free\css\all.min.css')}}" rel="stylesheet" type="text/css">
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        @include('objetos.navbar')

        @include('objetos.sidebar-esq')

        @yield('content')

        @include('objetos.sidebar-dir')

        @include('objetos.footer')
    </div>
    <script src="js/app.js"></script>
</body>

</html>