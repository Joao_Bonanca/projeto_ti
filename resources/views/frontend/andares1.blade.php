<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/app.css">
    <link href="{{ url('fontawesome-free\css\all.min.css')}}" rel="stylesheet" type="text/css">
</head>

<body>
    <nav class="top-left" aria-label="Page navigation example">
        <ul class="pagination">
            <li class="page-item"><a class="page-link" href="{{ route('andares0') }}"><i class="fas fa-caret-left"></i></a></li>
            <li class="page-item"><a class="page-link" href="{{ route('andares0') }}">Exterior e Garagem</a></li>
            <li class="page-item active"><a class="page-link" href="{{ route('andares1') }}">Rés do Chão</a></li>
            <li class="page-item"><a class="page-link" href="{{ route('andares2') }}">Primeiro andar</a></li>
            <li class="page-item"><a class="page-link" href="{{ route('andares2') }}"><i class="fas fa-caret-right"></i></a></li>
        </ul>
    </nav>
    <header>
        <div id="carouselExampleIndicators" class="carousel slide">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active" style="background-image: url('https://source.unsplash.com/LAaSoL0LrYs/1920x1080')">
                    <div class="carousel-caption d-none d-md-block"></div>
                </div>
                <div class="carousel-item" style="background-image: url('https://source.unsplash.com/bF2vsubyHcQ/1920x1080')">
                    <div class="carousel-caption d-none d-md-block"></div>
                </div>
                <div class="carousel-item" style="background-image: url('https://source.unsplash.com/szFUQoyvrxM/1920x1080')">
                    <div class="carousel-caption d-none d-md-block"></div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </header>

    <script src="js/app.js"></script>

</body>

</html>