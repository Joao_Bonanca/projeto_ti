<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/app.css">
    <link href="{{ url('fontawesome-free\css\all.min.css')}}" rel="stylesheet" type="text/css">
    <!-- <link rel="stylesheet" href="..\node_modules\admin-lte\plugins\ion-rangeslider\css\ion.rangeSlider.min.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/css/ion.rangeSlider.min.css" />
</head>

<body>
    <nav class="top-left" aria-label="Page navigation example">
        <ul class="pagination">
            <li class="page-item"><a class="page-link" href=""><i class="fas fa-caret-left"></i></a></li>
            <li class="page-item active"><a class="page-link" href="{{ route('andares0') }}">Exterior e Garagem</a></li>
            <li class="page-item"><a class="page-link" href="{{ route('andares1') }}">Rés do Chão</a></li>
            <li class="page-item"><a class="page-link" href="{{ route('andares2') }}">Primeiro andar</a></li>
            <li class="page-item"><a class="page-link" href="{{ route('andares1') }}"><i class="fas fa-caret-right"></i></a></li>
        </ul>
    </nav>
    <div class="card card-default top-right">
        <div class="card-header">
            <h4>Exterior da Casa</h4>
        </div>
        <div class="card-body">
            <h5>Luminosidade</h5>
            <input type="text" class="js-range-slider" name="my_range" value="" />
        </div>
    </div>

    <header>
        <div id="carouselExampleIndicators" class="carousel slide">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active" style="background-image: url(img/casa/exterior.jpg);">
                    <div class="carousel-caption d-none d-md-block"></div>
                </div>
                <div class="carousel-item" style="background-image: url('https://source.unsplash.com/bF2vsubyHcQ/1920x1080')">
                    <div class="carousel-caption d-none d-md-block"></div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </header>

    <script src="js/app.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/js/ion.rangeSlider.min.js"></script>
    <script>
        $(".js-range-slider").ionRangeSlider({
            type: "double",
            min: 0,
            max: 5,
            from: 0,
            to: 5,
            grid: true,
            skin: "flat",

        });
    </script>
</body>

</html>